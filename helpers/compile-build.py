#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to compile a project.')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--usingInstall', type=str, required=True)
arguments = parser.parse_args()

# Load our build specification, which governs how we handle this build
buildSpecification = BuildSpecs.Loader( product=arguments.product, project=arguments.project, branchGroup=arguments.branchGroup, platform=arguments.platform )

# Determine the environment we need to provide for the compilation process
buildEnvironment = EnvironmentHandler.generateFor( installPrefix=arguments.usingInstall )

# Determine where our source code is checked out to and where we will be building it
# We'll assume that the directory we're running from is where the sources are located
sourcesLocation = os.getcwd()
buildLocation = CommonUtils.buildDirectoryForSources( sources=sourcesLocation, inSourceBuild=buildSpecification['in-source-build'] )

# Determine the build command we want to use
# Just about all of our platforms support standard "make" so that is our default...
makeCommand = "make -j {cpuCount} -l {maximumLoad}"
# Windows is a bit special though
if sys.platform == 'win32':
	# We use NMake on Windows at the moment
	makeCommand = "ninja -j {cpuCount} -l {maximumLoad}"
# FreeBSD also likes to do things slightly different
if sys.platform == 'freebsd12':
	makeCommand = "gmake -j {cpuCount} -l {maximumLoad}"

# Compile the project
try:
	commandToRun = BuildSystem.substituteCommandTokens( makeCommand )
	subprocess.check_call( commandToRun, stdout=sys.stdout, stderr=sys.stderr, shell=True, cwd=buildLocation, env=buildEnvironment )
except Exception:
	sys.exit(1)

# The project was compiled successfully
sys.exit(0)
