#!/bin/bash
# Ensure we bail if anything fails
set -e

# Grab the various parameters which should have been passed to us
productName=$1
projectName=$2
currentPlatform=$3

# Fetch the system dependency command we will be running
systemDependencies=`python3 "$WORKSPACE/ci-tooling/helpers/getsetting.py" --product ${productName} --project ${projectName} --platform ${currentPlatform} --key externalDependencies`

# Ensure we'll be able to capture the dependency
export DESTDIR="$WORKSPACE/install-divert/"
export INSTALL_ROOT="$DESTDIR"

# Ensure the Android Tooling used in the system dependency commands knows what install prefix we want
export APP_INSTALL_PREFIX="$HOME/install-prefix/"

# Make sure the installation prefix exists
# If we don't, and there is no system dependency command, then nothing will create the directory which will cause rsync to fail later
mkdir -p "$DESTDIR/$APP_INSTALL_PREFIX/"

# Run the necessary system dependency command
sh -c "$systemDependencies"

# Transfer the System Dependencies we've just built into the general installation prefix so CMake finds them when it runs
# We leave them behind in the divert directory to ensure they're captured when the project itself is built normally
rsync -Hav "$DESTDIR/$APP_INSTALL_PREFIX/" "$APP_INSTALL_PREFIX/"
