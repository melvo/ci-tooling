#!/usr/bin/python3
import os
import sys
import copy
import argparse
import subprocess
from helperslib import BuildSpecs, CommonUtils

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to execute gcovr to extract LCov results (in Cobertura format).')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
arguments = parser.parse_args()

# Load our build specification, which governs how we handle running GCovr
buildSpecification = BuildSpecs.Loader( product=arguments.product, project=arguments.project, branchGroup=arguments.branchGroup, platform=arguments.platform )

# Determine where our source code is checked out to
# We'll assume that the directory we're running from is where the sources are located
sourcesLocation = os.getcwd()

# If we aren't running on Linux then bail as it's not known if GCovr works outside Linux
if sys.platform != "linux" or not buildSpecification['extract-lcov-results']:
	# Bail!
	sys.exit(0)

# Prepare the environment for usage
# We need to ensure we have a UTF-8 environment setup other the system will default to POSIX/C
# Unfortunately this means that Python will default to just ASCII coding for reading files, which isn't useful and will fail in some circumstances
executionEnvironment = copy.deepcopy(os.environ)
executionEnvironment['LANG'] = 'en_US.UTF-8'

# Determine the command we need to run
# We ask GCovr to exclude the build directory by default as we don't want generated artifacts (like moc files) getting included as well
# Sometimes projects will want to customise things slightly so we provide for that as well
commandToRun = 'gcovr -e "build/.*" -r "{sources}" {otherArguments}'
commandToRun = commandToRun.format( sources=sourcesLocation, otherArguments=buildSpecification['lcov-extractor-arguments'] )

# Now run it!
# If gcovr bails we ignore it, as failures to extract lcov results shouldn't cause builds to fail.
try:
	subprocess.check_call( commandToRun, stdout=sys.stdout, stderr=sys.stderr, shell=True, env=executionEnvironment )
except Exception:
	pass

# LCov extraction was successful
sys.exit(0)
